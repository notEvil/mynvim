local m_lua = require('mynvim.lua')
local m_search = require('mynvim.search')
local m_shared = require('mynvim.shared')


local export = {}


function export.find(before_backward)
  m_shared.echo('Character: ')
  local character = string.char(vim.fn.getchar())
  m_shared.echo('')
  if character == '\27' then  -- esc
    return
  end
  local pattern = m_lua.if_(character == '.', '\\[^a-zA-Z0-9 ]', character:gsub('\\', '\\\\'))
  if before_backward == nil then
    pattern = '\\V' .. pattern
  elseif before_backward then
    pattern = '\\V' .. pattern .. '\\zs\\_.'
  else
    pattern = '\\V\\_.' .. pattern
  end
  return function(backward) m_search.search(pattern, {backward=backward}) end
end


return export
