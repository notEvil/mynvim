local m_lua = require('mynvim.lua')
local m_shared = require('mynvim.shared')


local export = {}


function export.search(pattern, kwargs)
  if kwargs == nil then
    kwargs = {}
  end
  kwargs = m_lua.unite_tables(
    { backward = false, current = false, ['end'] = false, move = true },
    kwargs
  )

  local flags = 'W'
  if kwargs['backward'] then
    flags = flags .. 'b'
  end
  if kwargs['current'] then
    flags = flags .. 'c'
  end
  if kwargs['end'] then
    flags = flags .. 'e'
  end
  if not kwargs['move'] then
    flags = flags .. 'n'
  end
  return unpack(vim.fn.searchpos(pattern, flags))
end

function export.search_middle(
    backward, begin_pattern, begin_kwargs, end_pattern, end_kwargs
)
  begin_kwargs = m_lua.unite_tables(begin_kwargs, { backward = true, current = true })
  end_kwargs = m_lua.unite_tables(end_kwargs, { backward = false, current = true })

  if backward then
    local _a, _b = begin_pattern, begin_kwargs
    begin_pattern, begin_kwargs = end_pattern, end_kwargs
    end_pattern, end_kwargs = _a, _b
  end

  local current_line, current_column = m_shared.get_position()

  local begin_line, begin_column = export.search(begin_pattern, begin_kwargs)
  begin_kwargs['backward'] = backward
  if begin_line == 0 then
    begin_line, begin_column = export.search(begin_pattern, begin_kwargs)
    if begin_line == 0 then
      return nil
    end
  end

  local result_line, result_column
  while true do
    local b_line, b_column = export.search(end_pattern, end_kwargs)
    if b_line == 0 then
      m_shared.set_position(current_line, current_column)
      return nil
    end

    result_line = math.floor((b_line + begin_line) / 2)
    result_column = math.floor((b_column + begin_column) / 2)
    result_column = math.min(
      result_column, vim.fn.strwidth(vim.fn.getline(result_line))
    )
    local line_difference = result_line - current_line
    local column_difference = result_column - current_column
    if backward then
      line_difference = -line_difference
      column_difference = -column_difference
    end
    if 0 < line_difference or (line_difference == 0 and 0 < column_difference) then
      break
    end

    m_shared.set_position(begin_line, begin_column)
    begin_kwargs['current'] = false
    begin_line, begin_column = export.search(begin_pattern, begin_kwargs)
    begin_kwargs['current'] = true
    if begin_line == 0 then
      m_shared.set_position(current_line, current_column)
      return nil
    end
  end
  m_shared.set_position(result_line, result_column)
end

return export
