local m_lua = require('mynvim.lua')
local m_shared = require('mynvim.shared')


local export = {}
local last_move = nil


function export.move(backward, move)
  if move == nil then
    move = last_move
    if move == nil then
      return
    end
  end

  local success, object = pcall(function() move(backward) end)
  if not success then
    vim.api.nvim_err_writeln(m_shared.truncate_message(m_shared.remove_path(object)))
  end
  last_move = move
end


function export.after_search(backward)
  local forward_key, backward_key
  if backward then
    forward_key, backward_key = 'N', 'n'
  else
    forward_key, backward_key = 'n', 'N'
  end
  last_move = function(backward) m_shared.feedkeys(m_lua.if_(backward, backward_key, forward_key)) end
end


return export
