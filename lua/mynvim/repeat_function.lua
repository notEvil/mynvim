local m_lua = require('mynvim.lua')
local m_shared = require('mynvim.shared')


local export = {}
local last_functions = {}
local last_mode = nil
local dont_repeat = true


function export.call(function_, repeat_functions)
  local mode = vim.api.nvim_get_mode().mode
  if function_ == nil then
    function_ = last_functions[mode]
    if function_ == nil then
      if dont_repeat or next(last_functions) ~= nil then
        return
      end

      if m_shared.is_visual_mode(mode) then
        m_shared.feedkeys(':normal! .<cr>')
      else
        m_shared.feedkeys('.')
      end
      return
    end

    mode = last_mode
  else
    last_functions = m_lua.if_(
      repeat_functions == nil, { [mode] = function_ }, repeat_functions
    )
    last_mode = mode
    dont_repeat = false
  end

  function_(mode)
end

function export.calling(function_, repeat_functions)
  return function() export.call(function_, repeat_functions) end
end

function export.feeding(keys, remap, asis)
  return export.calling(function() m_shared.feedkeys(keys, remap, asis) end)
end

function export.dot(argument)
  return function()
    last_functions = {}
    last_mode = nil
    dont_repeat = false
    if type(argument) == 'string' then
      m_shared.feedkeys(argument)
    else
      argument()
    end
  end
end

function export.dont(argument)
  return function()
    last_functions = {}
    last_mode = nil
    dont_repeat = true
    if type(argument) == 'string' then
      m_shared.feedkeys(argument)
    else
      argument()
    end
  end
end

return export
