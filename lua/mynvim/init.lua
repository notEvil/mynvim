local m_find = require('mynvim.find')
local m_lua = require('mynvim.lua')
local m_repeat_function = require('mynvim.repeat_function')
local m_repeat_move = require('mynvim.repeat_move')
local m_search = require('mynvim.search')
local m_shared = require('mynvim.shared')


local function s(string)
  local _w = '[0-9A-Za-z_äöüÄÖÜß]'
  local _W = '[^0-9A-Za-z_äöüÄÖÜß]'
  local _WS = '[^0-9A-Za-z_äöüÄÖÜß \\t\\n]' -- [^\w\s]
  return string:gsub('\\w', _w):gsub('\\WS', _WS):gsub('\\W', _W)
end

local OBJECT_PATTERNS = {
  ['w'] = { s('\\v(^|\\W)@<=\\w'), s('\\v\\w(\\W|$)@=') },
  ['W'] = {
    s('\\v(^|\\W)@<=\\w'), s('\\v(\\w(\\WS|$)@=)|((\\w\\s*)@<=\\s(\\S|$)@=)')
  },
  ['b'] = { '(', ')' },
  ['B'] = { '(', '\\v(\\)(\\S|$)@=)|((\\)\\s*)@<=\\s(\\S|$)@=)' },
  ['['] = { '[', ']' },
  [']'] = { '[', '\\v(\\](\\S|$)@=)|((\\]\\s*)@<=\\s(\\S|$)@=)' },
  ['{'] = { '{', '}' },
  ['}'] = { '{', '\\v(\\}(\\S|$)@=)|((\\}\\s*)@<=\\s(\\S|$)@=)' },
  ['<'] = { '<', '>' },
  ['>'] = { '<', '\\v(\\>(\\S|$)@=)|((\\>\\s*)@<=\\s(\\S|$)@=)' },
  ['p'] = {
    '\\v(%^(\\s*\\n)?|\\n\\s*\\n)\\s*\\zs\\S',
    '\\v\\S\\s*(\\n\\s*\\n|(\\n\\s*)?%$)',
  },
  ['P'] = {
    '\\v(%^(\\s*\\n)?|\\n\\s*\\n)\\zs\\s*\\S',
    '\\v\\S\\s*(\\n\\s*\\zs\\n|(\\n\\s*)?\\zs%$)',
  },
  ['l'] = { '\\v^\\s*\\zs\\S', '\\v\\S\\s*$' },
  ['L'] = { '\\v^\\s*\\S', '\\v\\S\\s*\\zs$' },
  ['c'] = {
    '\\v(%^|(^([^+-].*)?\\n))@<=([+-])@=', '\\v^[+-].*(\\zs(\\n([^+-])@=)|(\\n?%$))'
  },
}
local STEP_PATTERN = s(
  '\\v%(%(^|\\W)@<=\\w)|%(%(^|\\w|\\s)@<=\\WS)|%(\\w%(\\W|$)@=)|%(\\WS%(\\w|\\s|$)@=)'
)


local export = {}

local calling = m_repeat_function.calling
local feeding = m_repeat_function.feeding
local dot = m_repeat_function.dot
local dont = m_repeat_function.dont

local append_selection
local create_mappings
local cycle_visual_mode
local get_paste
local get_select_object
local goto_line
local join
local like
local map_search
local map_search_middle
local on_escape_command
local on_escape_insert
local on_normal_enter
local on_submit_command
local paste
local prepend_selection
local remove_mapping
local replay_macro
local scroll

local prepare_mappings = true
local mappings = {}
local restore_pattern = false
local selection = nil


function export.initialize(colemak)
  if colemak == nil then
    colemak = false
  end

  vim.g.no_plugin_maps = 1

  if vim.fn.has('vim_starting') then
    vim.api.nvim_create_autocmd('VimEnter', { callback = create_mappings })
  else
    create_mappings()
  end

  -- keep cursor in center
  vim.opt.scrolloff = 999
  vim.api.nvim_create_autocmd('WinEnter', {
    callback = function()
      if vim.api.nvim_win_get_config(0).relative ~= nil then
        vim.api.nvim_win_set_option(0, 'scrolloff', 999)
      end
    end
  })
  --
  for number in ('0123456789'):gmatch('.') do
    export.map(number, nil, 'n')
  end
  --

  -- navigation
  export.map('<cr>', on_normal_enter, 'n')
  -- absolute
  export.map('gl', goto_line, { 'n', 'v' })
  export.map('Sf', 'gg', { 'n', 'v' })
  export.map('mf', function()
    m_shared.set_position(math.ceil(vim.fn.line('$') / 2))
  end, { 'n', 'v' })
  export.map('ef', 'G', { 'n', 'v' })
  -- vim-easymotion
  export.map('t', '<Plug>(easymotion-overwin-f2)', 'n', { remap = true })
  export.map('t', '<Plug>(easymotion-bd-f2)', 'v', { remap = true })
  -- relative
  export.map('w', function() m_search.search(STEP_PATTERN) end, { 'n', 'v' })
  export.map('W', function()
    m_search.search(STEP_PATTERN, { backward = true })
  end, { 'n', 'v' })
  export.map('f', function()
    m_repeat_move.move(false, m_find.find())
  end, { 'n', 'v' })
  export.map('F', function() m_repeat_move.move(true, m_find.find()) end, { 'n', 'v' })
  export.map('b', function()
    m_repeat_move.move(false, m_find.find(false))
  end, { 'n', 'v' })
  export.map('B', function()
    m_repeat_move.move(true, m_find.find(true))
  end, { 'n', 'v' })
  for object, patterns in pairs(OBJECT_PATTERNS) do
    map_search('s' .. object, 'S' .. object, patterns[1])
    map_search_middle('m' .. object, 'M' .. object, patterns[1], {}, patterns[2], {})
    map_search('e' .. object, 'E' .. object, patterns[2])
  end
  export.map_move('sv', 'Sv', function(backward)
    scroll(m_lua.if_(backward, -0.5, 0.5))
  end)
  export.map_move('mv', 'Mv', function(backward)
    scroll(m_lua.if_(backward, -1, 1))
  end)
  export.map_move('ev', 'Ev', function(backward)
    scroll(m_lua.if_(backward, -0.5, 0.5))
  end)
  export.map_move('sm', 'Sm', function(backward)
    m_shared.feedkeys(m_lua.if_(backward, 'N', 'n'))
  end)
  map_search_middle('mm', 'Mm', '', {}, '', { ['end'] = true })
  map_search('em', 'Em', '', { ['end'] = true })
  export.map_move('se', 'Se', function(backward)
    m_shared.execute_command(m_lua.if_(backward, 'cp', 'cn'))
  end)
  -- repeat
  export.map('n', function() m_repeat_move.move(false) end, { 'n', 'v' })
  export.map('N', function() m_repeat_move.move(true) end, { 'n', 'v' })
  -- history
  export.map('h', '<c-o>', 'n')
  export.map('H', '<c-i>', 'n')
  -- fallback
  export.map('<a-h>', '<left>', { 'n', 'v', 'c' })
  export.map('<a-' .. m_lua.if_(colemak, 'n', 'j') .. '>', '<down>', { 'n', 'v', 'c' })
  export.map('<a-' .. m_lua.if_(colemak, 'e', 'k') .. '>', '<up>', { 'n', 'v', 'c' })
  export.map(
    '<a-' .. m_lua.if_(colemak, 'i', 'l') .. '>', '<right>', { 'n', 'v', 'c' }
  )
  --

  -- search
  export.map('l', function() like(false) end, 'n')
  export.map('L', function() like(true) end, 'n')
  export.map('/', '/\\v', { 'n', 'v' })
  export.map('?', '?\\v', { 'n', 'v' })
  -- selected
  export.map('l', function() like(false) end, 'v')
  export.map('L', function() like(true) end, 'v')
  --

  -- select
  export.map('v', cycle_visual_mode, 'v')
  export.map('<esc>', '<esc>``', 'v') -- requires m` on enter
  export.map('V', 'm`o', 'v')
  -- enter
  export.map('v', 'm`v', 'n')
  export.map('V', 'm`gv', 'n')
  -- objects
  for character in ('b[{<'):gmatch('.') do
    export.map('o' .. character, 'm`va' .. character, 'n')
    export.map('o' .. character, 'a' .. character, 'v')
  end
  for character in ('\'"`'):gmatch('.') do
    export.map('o' .. character, 'm`v2i' .. character, 'n')
    export.map('o' .. character, '2i' .. character, 'v')
  end
  for object in ('wWB]}>lLpPmc'):gmatch('.') do
    export.map('o' .. object, get_select_object(object, false), 'n')
    export.map('o' .. object, get_select_object(object, true), 'v')
  end
  --

  -- copy
  export.map('y', nil, 'v')
  export.map('<a-y>', '"+y', 'v')
  export.map('<c-y>', function()
    vim.ui.input({ prompt = 'Copy to Register: ' }, function(string)
      if string ~= nil then
        m_shared.feedkeys('m`gv"' .. string .. 'y')
      end
    end)
  end, 'v')
  --

  -- change
  -- insert
  export.map('i', function()
    m_repeat_function.call(
      function() m_shared.feedkeys('i') end,
      { n = function() m_shared.feedkeys('.`]') end }
    )
  end, 'n')
  export.map('I', function()
    m_repeat_function.call(
      function() m_shared.feedkeys('a') end,
      { n = function() m_shared.feedkeys('.`]') end }
    )
  end, 'n')
  export.map('a', dot('A'), 'n')
  export.map('A', dot('I'), 'n')
  export.map('<a-a>', dot('o'), 'n')
  export.map('<a-A>', dot('O'), 'n')
  export.map('a', function()
    m_repeat_function.call(append_selection, {
      n = function(mode)
        if mode == 'v' then
        elseif mode == 'V' then
          local line, column = m_shared.get_position()
          m_shared.feedkeys('.')
          m_shared.set_position(line, column)
          m_shared.feedkeys('$')
        else
          local line, _ = m_shared.get_position()
          m_shared.feedkeys('.`]')
          local _, column = m_shared.get_position()
          m_shared.set_position(line, column)
        end
      end
    })
  end, 'v')
  export.map('A', function()
    m_repeat_function.call(prepend_selection, {
      n = function(mode)
        if mode == 'v' then
          m_shared.feedkeys('.`]')
        else
          local line, _ = m_shared.get_position()
          m_shared.feedkeys(m_lua.if_(mode == 'V', '^', '') .. '.`]')
          local _, column = m_shared.get_position()
          m_shared.set_position(line, column)
        end
      end
    })
  end, 'v')
  export.map('<esc>', on_escape_insert, 'i')
  -- paste
  export.map('p', get_paste('gP'), 'n')
  export.map('<a-p>', get_paste('"+gP'), 'n')
  export.map('P', get_paste('m`p``'), 'n')
  export.map('<a-P>', get_paste('m`"+p``'), 'n')
  -- replace
  export.map('i', dot('c'), 'v')
  export.map('p', get_paste('gP'), 'v')
  export.map('<a-p>', get_paste('"+gP'), 'v')
  export.map('R', dont(':%s///g<left><left>'), 'n')
  export.map('R', dont(':s///g<left><left>'), 'v')
  -- delete
  export.map('d', dot('d'), 'v')
  -- shift
  export.map(',', dot('>>'), 'n')
  export.map(',', dot('1>'), 'v')
  export.map('<', dot('<<'), 'n')
  export.map('<', dot('1<'), 'v')
  -- join
  export.map('j', dont(join), 'v')
  -- queue
  export.map('Q', 'q', 'n')
  export.map('q', replay_macro, { 'n', 'v' })
  -- undo/redo
  export.map('u', nil, 'n')
  export.map('U', '<c-r>', 'n')
  --

  -- command
  export.map('c', dot(':'), { 'n', 'v' })
  export.map('<cr>', on_submit_command, 'c')
  export.map('<esc>', on_escape_command, 'c')
  export.map('<c-u>', nil, 'c')
  -- autocomplete
  export.map('<tab>', nil, 'c')
  export.map('<s-tab>', nil, 'c')
  --

  -- terminal
  export.map('<esc>', nil, 't')
  --

  -- input
  for mode in ('cit'):gmatch('.') do
    for character in ('`1234567890-=' .. '~!@#$%^&*()_+' ..
      'qwertyuiop[]' .. 'QWERTYUIOP{}' ..
      "asdfghjkl;'" .. 'ASDFGHJKL:"' ..
      '\\zxcvbnm,./' .. '|ZXCVBNM<>?'):gmatch('.') do
      export.map(character, nil, mode)
    end
    for _, trigger in pairs({ '<space>', '<bs>' }) do
      export.map(trigger, nil, mode)
    end
  end
  export.map('<tab>', nil, { 'i', 't' })
  export.map('<cr>', nil, { 'i', 't' })
  -- paste
  export.map('<a-p>', '<c-r>"', { 'c', 'i', 't' })
  export.map('<a-P>', '<c-r>+', { 'c', 'i', 't' })
  export.map('<c-p>', '<c-r>', { 'c', 'i', 't' })
  --

  export.map('r', m_repeat_function.call, { 'n', 'v' })
  export.map('<a-r>', m_repeat_function.call, { 'c', 'i', 't' })
end

create_mappings = function()
  prepare_mappings = false

  local modes = { 'n', 'v', 'x', 's', 'o', 'i', 'l', 'c', 't' }

  -- remove existing mappings
  for _, mode in pairs(modes) do
    for _, mapping in pairs(vim.api.nvim_get_keymap(mode)) do
      if mapping.lhs:find('^<Plug>') == nil then
        remove_mapping(mapping.lhs, mode)
      end
    end
  end

  -- map everything to <nop>
  for _, pair in pairs(
    { { '', '' }, { '<c-', '>' }, { '<s-', '>' }, { '<a-', '>' } }
  ) do
    local prefix, suffix = unpack(pair)
    for trigger in ('`1234567890-=' .. '~!@#$%^&*()_+' ..
      'qwertyuiop[]' .. 'QWERTYUIOP{}' ..
      "asdfghjkl;'" .. 'ASDFGHJKL:"' ..
      '\\zxcvbnm,./' .. '|ZXCVBNM<>?'):gmatch('.') do
      export.map(prefix .. trigger .. suffix, '<nop>', modes)
    end
  end
  for _, pair in pairs(
    { { '<', '>' }, { '<s-', '>' }, { '<c-', '>' }, { '<s-', '>' }, { '<a-', '>' } }
  ) do
    local prefix, suffix = unpack(pair)
    for _, trigger in pairs({ 'esc', 'bs', 'tab', 'cr', 'space' }) do
      export.map(prefix .. trigger .. suffix, '<nop>', modes)
    end
  end

  -- create mappings
  for _, mapping in pairs(mappings) do
    export.map(unpack(mapping, 1, 4))
  end

  mappings = nil
end


on_normal_enter = function()
  if vim.fn.win_gettype() == 'quickfix' then
    local first = true
    m_repeat_move.move(false, function(backward)
      if first then
        first = false
        vim.fn.execute('.cc')
      else
        m_shared.execute_command(m_lua.if_(backward, 'cp', 'cn'))
      end
    end)
  end
end


goto_line = function()
  vim.ui.input({ prompt = 'Goto Line: ' }, function(string)
    if string ~= nil then
      m_shared.set_position(tonumber(string))
    end
  end)
end


map_search = function(forward_trigger, backward_trigger, pattern, kwargs)
  if kwargs == nil then
    kwargs = {}
  end
  local backward_kwargs = m_lua.unite_tables(kwargs, { backward = true })
  export.map_move(forward_trigger, backward_trigger, function(backward)
    m_search.search(pattern, m_lua.if_(backward, backward_kwargs, kwargs))
  end)
end


map_search_middle = function(
    forward_trigger,
    backward_trigger,
    start_pattern,
    start_kwargs,
    end_pattern,
    end_kwargs
)
  export.map_move(forward_trigger, backward_trigger, function(backward)
    m_search.search_middle(
      backward, start_pattern, start_kwargs, end_pattern, end_kwargs
    )
  end)
end


scroll = function(factor)
  local line, column = m_shared.get_position()
  local first_line = vim.fn.line('w0')
  local last_line = vim.fn.line('w$')
  if factor == 0.5 then
    line = last_line
  elseif factor == -0.5 then
    line = first_line
  elseif factor == 1 then
    line = last_line + math.floor((last_line - first_line) / 2) + 1
  elseif factor == -1 then
    line = first_line - math.ceil((last_line - first_line) / 2) - 1
  else
    line = line
        + m_lua.if_(factor < 0, -1, 1)
        * math.floor((last_line - first_line + 1) * math.abs(factor))
  end
  if line < 1 then
    line = 1
  elseif vim.fn.line('$') < line then
    line = vim.fn.line('$')
  end
  m_shared.set_position(line, column)
end


like = function(backward)
  local normal_mode = vim.api.nvim_get_mode().mode == 'n'
  local selection
  if normal_mode then
    selection = { m_shared.get_selection() }
    m_shared.feedkeys('ow', true)
  end
  local pattern = vim.fn.escape(m_shared.get_selected_text(), '\\/'):gsub('\n', '\\n')
  m_shared.feedkeys('<esc>``')
  if normal_mode then
    pattern = '\\<' .. pattern .. '\\>'
    m_shared.set_selection(unpack(selection))
  end
  m_shared.feedkeys(
    m_lua.if_(backward, '?', '/')
    .. '\\V'
    .. pattern
    .. m_shared.replace_termcodes('<cr>'),
    true,
    true
  )
end


cycle_visual_mode = function()
  local mode = vim.api.nvim_get_mode().mode
  m_shared.feedkeys(m_lua.if_(mode == 'v', 'V', m_lua.if_(mode == 'V', '<c-v>', 'v')))
end


get_select_object = function(object, visual)
  return function()
    if not visual then
      -- m`
      local line, column = m_shared.get_position()
      m_shared.set_position(line, column, '`')
    end

    local start_pattern, end_pattern
    local start_kwargs = { backward = true, current = true }
    local end_kwargs = { current = true }
    if object == 'm' then
      start_pattern, end_pattern = '', ''
      end_kwargs['end'] = true
    else
      start_pattern, end_pattern = unpack(OBJECT_PATTERNS[object])
    end

    if visual then
      -- preserve side
      local line, column = m_shared.get_position()
      m_shared.feedkeys('o')
      local opposite_line, opposite_column = m_shared.get_position()
      m_shared.feedkeys('o')
      if line < opposite_line
          or (line == opposite_line and column < opposite_column)
      then
        start_pattern, start_kwargs, end_pattern, end_kwargs =
            end_pattern, end_kwargs, start_pattern, start_kwargs
      end
    end

    local line, column = m_search.search(end_pattern, end_kwargs)
    if visual then
      m_shared.feedkeys('o')
      m_shared.set_position(line, column)
    else
      m_shared.feedkeys(m_lua.if_(('pP'):find(object) == nil, 'v', 'V'))
      -- could be end of line
      m_shared.set_position(line, column)
      m_shared.feedkeys('o')
    end
    m_search.search(start_pattern, start_kwargs)
    m_shared.feedkeys('o')
  end
end


append_selection = function()
  selection = { { m_shared.get_selection() }, false }
  local mode = vim.api.nvim_get_mode().mode
  m_shared.feedkeys(
    m_lua.if_(mode == 'v', '<esc>`>a', m_lua.if_(mode == 'V', '<c-v>$A', 'A'))
  )
end


prepend_selection = function()
  selection = { { m_shared.get_selection() }, true }
  local mode = vim.api.nvim_get_mode().mode
  m_shared.feedkeys(
    m_lua.if_(mode == 'v', '<esc>`<i', m_lua.if_(mode == 'V', '<c-v>^I', 'I'))
  )
end


on_escape_insert = function()
  m_shared.feedkeys('<esc>`^')

  if selection ~= nil then -- restore selection
    local t, prepend = unpack(selection)
    local mode, left_position, right_position = unpack(t)

    vim.schedule(function()
      if prepend then
        local _, column = m_shared.get_position()
        local offset = column - math.min(left_position[2], right_position[2])

        m_shared.set_selection(
          mode,
          { left_position[1], left_position[2] + offset },
          {
            right_position[1],
            right_position[2] + m_lua.if_(
              (mode == 'v' and right_position[1] == left_position[1])
              or mode == m_shared.CTRL_V,
              offset,
              0
            )
          }
        )
      else
        m_shared.set_selection(mode, left_position, right_position)
      end
    end)

    selection = nil
  end
end


get_paste = function(keys)
  return calling(function()
    local string = vim.fn.getreg('')
    m_shared.feedkeys(keys)
    vim.fn.setreg('', string)
    local line, column = m_shared.get_position('[')
    m_shared.set_position(line, column, '<')
    line, column = m_shared.get_position(']')
    m_shared.set_position(line, column, '>')
  end)
end


join = function()
  restore_pattern = true
  m_shared.feedkeys(':s/\\v\\S\\zs\\s*\\n\\s*\\ze\\S//g<left><left>')
end


replay_macro = function()
  m_shared.echo('Macro: ')
  local character = string.char(vim.fn.getchar())
  m_shared.echo('')
  if character == m_shared.ESC then
    return
  end
  feeding(':normal! @' .. character .. '<cr>')()
end


on_submit_command = function()
  vim.api.nvim_feedkeys(
    m_shared.replace_termcodes('<cr>'), 'n', false
  ) -- m_shared.feedkeys breaks visual mode

  local type_string = vim.fn.getcmdtype()
  if ('/?'):find(type_string) ~= nil then
    m_repeat_move.after_search(type_string == '?')
  end
  if restore_pattern then
    local pattern = vim.fn.getreg('/')
    vim.schedule(function() vim.fn.setreg('/', pattern) end)
    restore_pattern = false
  end
end


on_escape_command = function()
  m_shared.feedkeys('<esc>')
  restore_pattern = false
end


function export.map(trigger, event, mode, options)
  if prepare_mappings then
    table.insert(mappings, { trigger, event, mode, options })
    return
  end

  if event == nil then
    remove_mapping(trigger, mode)
    return
  end
  if options == nil then
    options = {}
  end
  m_lua.unite_tables({ silent = true }, options)
  vim.keymap.set(mode, trigger, event, options)
end

remove_mapping = function(trigger, mode)
  local success, object = pcall(function()
    vim.keymap.del(mode, trigger)
  end)
  assert(success or object:find('No such mapping') ~= nil)
end


function export.map_move(forward_trigger, backward_trigger, move, mode)
  if mode == nil then
    mode = { 'n', 'v' }
  end
  export.map(forward_trigger, function()
    m_repeat_move.move(false, move)
  end, mode)
  export.map(backward_trigger, function()
    m_repeat_move.move(true, move)
  end, mode)
end


return export
