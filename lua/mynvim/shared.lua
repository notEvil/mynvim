local m_lua = require('mynvim.lua')


local export = {}


function export.replace_termcodes(string)
  return vim.api.nvim_replace_termcodes(string, true, true, true)
end

export.ESC = export.replace_termcodes('<esc>')
export.CTRL_V = export.replace_termcodes('<c-v>')


function export.is_visual_mode(mode)
  if mode == nil then
    mode = vim.api.nvim_get_mode().mode
  end
  return mode:find('[vV' .. export.CTRL_V .. ']') ~= nil
end

function export.feedkeys(string, remap, asis)
  --[[
  - in command and insert mode, enqueues string
    - can't "execute" in these modes, things break
    - workaround: feed keys and schedule function call
  - in every other mode, processes string
  - IMPORTANT: It is NOT possible to exit insert mode! Workaround: schedule function call
  --]]
  if asis == nil then
    asis = false
  end
  if remap == nil then
    remap = false
  end
  local mode = m_lua.if_(remap, '', 'n')
  if vim.api.nvim_get_mode().mode:find('[cit]') ~= nil then
    mode = mode .. 't'
  else
    mode = mode .. 'x!'
  end
  if not asis then
    string = export.replace_termcodes(string)
  end
  vim.api.nvim_feedkeys(string, mode, false)
end


function export.execute_normal(string, remap)
  if remap == nil then
    remap = false
  end
  export.execute_command('normal', { export.replace_termcodes(string) }, not remap)
end

function export.execute_command(command, arguments, bang)
  if bang == nil then
    bang = false
  end
  vim.api.nvim_cmd({ cmd = command, bang = bang, args = arguments }, {})
end

function export.get_position(mark)
  local line, column
  if mark == nil then
    line, column = unpack(vim.api.nvim_win_get_cursor(0))
  else
    line, column = unpack(vim.api.nvim_buf_get_mark(0, mark))
  end
  return line, column + 1
end

function export.set_position(line, column, mark)
  if column == nil then
    local _
    _, column = export.get_position(mark)
  end
  if mark == nil then
    vim.api.nvim_win_set_cursor(0, { line, column - 1 })
  else
    vim.api.nvim_buf_set_mark(0, mark, line, column - 1, {})
  end
end

function export.get_selection()
  local mode = vim.api.nvim_get_mode().mode
  if export.is_visual_mode(mode) then
    local position = vim.fn.getpos('v')
    return mode, { position[2], position[3] }, { export.get_position() }
  end
  return vim.fn.visualmode(), { export.get_position('<') }, { export.get_position('>') }
end

function export.set_selection(mode, left_position, right_position)
  local current_mode = vim.api.nvim_get_mode().mode
  if current_mode == 'n' then
  elseif export.is_visual_mode(current_mode) then
    export.feedkeys('<esc>')
  else
    return
  end
  export.feedkeys(mode .. export.ESC, false, true)
  export.set_position(left_position[1], left_position[2], '<')
  export.set_position(right_position[1], right_position[2], '>')
  if current_mode ~= 'n' then
    export.feedkeys('gv')
  end
end

function export.get_selected_text()
  local _, left_position, right_position = export.get_selection()
  local lines
  if right_position[2] == (vim.v.maxcol + 1) then
    lines = vim.api.nvim_buf_get_lines(0, left_position[1] - 1, right_position[1], true)
  else
    lines = vim.api.nvim_buf_get_text(
      0,
      left_position[1] - 1,
      left_position[2] - 1,
      right_position[1] - 1,
      right_position[2],
      {}
    )
  end
  return table.concat(lines, '\n')
end

function export.echo(string)
  vim.api.nvim_echo({ { string } }, false, {})
end

function export.truncate_message(string, middle)
  local width = vim.api.nvim_win_get_width(0) - 1
  if string:len() <= width then
    return string
  end
  if middle == nil then
    return '<' .. string:sub(-(width - 1), -1)
  else
    local half = math.floor(width / 2)
    return string:sub(1, half - 1) .. '..' .. string:sub(-(width - half - 1), -1)
  end
end

function export.remove_path(string)
  return string:gsub('^.*%.lua:%d+: ', '')
end

return export
