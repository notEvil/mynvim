local export = {}


function export.if_(bool, true_object, false_object)
  if bool then
    return true_object
  end
  return false_object
end


function export.get(object, default)
  return export.if_(object == nil, default, object)
end


function export.get_key(table, value)
  for table_key, table_value in pairs(table) do
    if table_value == value then
      return table_key
    end
  end
  return nil
end


function export.copy_table(table)
  local result = {}
  for key, value in pairs(table) do
    result[key] = value
  end
  return result
end


function export.update_table(table, with_table)
  for key, value in pairs(with_table) do
    table[key] = value
  end
end


function export.unite_tables(table, ...)
  local result = export.copy_table(table)
  for _, table in pairs({...}) do
    export.update_table(result, table)
  end
  return result
end


function export.split_string(string, pattern)
  local result = {}
  local index = 1
  while true do
    local start_index, end_index = string:find(pattern, index)
    if start_index == nil then
      break
    end
    table.insert(result, string:sub(index, start_index - 1))
    index = end_index + 1
  end
  table.insert(result, string:sub(index))
  return result
end


function export.are_equal(first_table, second_table)
  if table.getn(first_table) ~= table.getn(second_table) then
    return false
  end
  for key, first_object in pairs(first_table) do
    if second_table[key] ~= first_object then
      return false
    end
  end
  return true
end


return export
