Don't mind the name, please ...

# mynvim

is a redesign of [_Vim_](https://github.com/vim/vim)/[_Neovim_](https://github.com/neovim/neovim)s default key mappings.
It is heavily opinionated and by no means "perfect" or "optimal".
If you think vanilla _Vim_/_Neovim_ is not expressive enough and default key mappings get in your way, _mynvim_ might interest you.

## Features

- Easy to learn
  - `sv`, `n`/`N`, `t`, `v` and `i` already get you far
  - keys have a single purpose like "move", "select" or "change"
- Simple concepts
  - e.g. forward/BACKWARD, `{move}{select}{change}` paradigm and repeatable move/change
- More expressive than vanilla _Vim_/_Neovim_
  - e.g. forward/backward repeatable "till" (`b`/`B`), move to middle or end of objects (`m`/`M`, `e`/`E`) and join lines (`j`)

## Quickstart

- Install _mynvim_ and [_EasyMotion_](https://github.com/easymotion/vim-easymotion) as plugins
  - see [`example_init.lua`](./example_init.lua)
- Add `require('mynvim').initialize()` early in your `init.lua`
- Use `require('mynvim').map` instead of `vim.keymap.set` or equivalent
  - necessary because _mynvim_ will disable all key mappings on `VimEnter`

## Documentation

- see [layout.colemak.ods](./layout.colemak.ods), [layout.colemak.pdf](./layout.colemak.pdf), [layout.qwerty.ods](./layout.qwerty.ods) or [layout.qwerty.pdf](./layout.qwerty.pdf)
- Annotations
  - _N_: in Normal mode
  - _V_: in Visual mode
  - _C_: in Command mode
  - _I_: in Insert mode

### Move

**Absolute**

- `t` in _NV_

  Asks for two characters, then shows labels at matching locations and asks for a label, then moves to the corresponding location.

- `gl` in _NV_

  Asks for a number, then moves to the corresponding line.

**Relative**

- `Alt+h`, `Alt+n`, `Alt+e`, `Alt+i` in _NVC_

  Moves the cursor left, down, up or right.

- `w`/`W` in _NV_

  Moves the cursor a small step forward/backward.

- `f`/`F` in _NV_

  Asks for a character, then moves to the next/previous search match.

- `b`/`B` in _NV_

  Asks for a character, then moves to the character before the next/previous match.
  The character "before" the previous match is the character after the match.

- `s{object}`/`S{object}` in _NV_

  Moves to the start of the next/previous text object.

- `m{object}`/`M{object}` in _NV_

  Moves to the middle of the next/previous text object.

- `e{object}`/`E{object}` in _NV_

  Moves to the end of the next/previous text object.

**Other**

- `n`/`N` in _NV_

  Repeats the last move forward/backward.

- `h`/`H` in _N_

  Moves backward/forward in history.

- `Enter` in Quickfix

  Moves to the Quickfix entry under the cursor.
  Then, `n`/`N` moves to the next/previous entry.

### Select

- `v` in _N_

  Enters Visual mode.

- `v` in _V_

  Cycles through Visual modes.

- `o{target}` in _NV_

  Selects a target.
  Target is either one of `` b[{`'" `` or a text object.
  `` b[{`'" `` correspond to matching (), [], {}, \`\`, '' and "".

- `V` in _N_

  Selects the last selection.

- `V` in _V_

  Switches side.

### Change

- `i`/`I` in _N_

  Enters Insert mode before/after the cursor.

- `i` in _V_

  Deletes selected text and enters Insert mode.

- `a`/`A` in _NV_

  Enters Insert mode at the end/start of the line or selection.
  In linewise or blockwise Visual mode, inserts the text on each line.

- `Alt+a`/`Alt+A` in _N_

  Enters Insert mode at a new line below/above.

- `,`/`<` in _NV_

  Shifts the line or selected lines right/left.

- `p`/`P` in _N_

  Pastes the text in register `"` before/after the cursor.

- `Alt+p`/`Alt+P` in _N_

  Pastes the text in register `+` before/after the cursor.

- `p` in _V_

  Replaces the selected text with the text in register `"`.

- `Alt+p` in _V_

  Replaces the selected text with the text in register `+`.

- `Alt+p` in _CI_

  Pastes the text in register `"`.

- `Alt+P` in _CI_

  Pastes the text in register `+`.

- `Ctrl+p` in _CI_

  Asks for a register and pastes the text in this register.

- `R` in _NV_

  Replaces search matches.
  In Visual mode, only within selected text.

- `d` in `V`

  Deletes the selected text.

- `j` in _V_

  Joins the selected lines.

- `Q` in _N_

  If not recording, asks for a character, then starts recording a macro.
  If recording, stops recording the macro.

- `q` in _NV_

  Asks for a character, then replays the corresponding macro.
  In Visual mode, the macro is replayed on each line.

- `u`/`U` in _N_

  Undoes last/redoes first change.

### Search

- `l`/`L` in _N_

  Searches the word under the cursor and moves to the next/previous match.
  Then, `n`/`N` moves to the next/previous match.

- `l`/`L` in _V_

  Searches the selected text and moves to the next/previous match.
  Then, `n`/`N` moves to the next/previous match.

- `/`/`?` in _NV_

  Initiates a search forward/backward.
  Then, `n`/`N` moves to the next/previous match.

### Other

- `r` in _NV_ and `Alt+r` in _CI_

  Repeats the last action which is not a move and usually a change.

- `y` in _V_

  Copies selected text to register `"`.

- `Alt+y` in _V_

  Copies selected text to register `+`.

- `Ctrl+y` in _V_

  Asks for a register and copies selected text to this register.

- `c` in _NV_

  Enters Command mode.

### Text Objects

- `w`: "word" or separating text
- `W`: "WORD" or separating text
- `b`: matching `(` `)`
- `B`: closest `(` `)` with trailing whitespace
- `[`: matching `[` `]`
- `]`: closest `[` `]` with trailing whitespace
- `{`: matching `{` `}`
- `}`: closest `{` `}` with trailing whitespace
- `<`: matching `<` `>`
- `>`: closest `<` `>` with trailing whitespace
- `l`: line w/o leading and trailing whitespace
- `L`: line w/ leading and trailing whitespace
- `p`: paragraph w/o trailing whitespace
- `P`: paragraph w/ trailing whitespace
- `m`: match of current search
- `c`: change in a unified diff
- `v`: part of file separated by view boundaries
- `f`: file
- `e`: QuickFix item

In general: upper case text objects are lower case text objects with surrounding whitespace.

## Background

Ten years ago I started to learn _Vim_ and switched from QWERTZ to [Colemak](https://colemak.com/).
*Vim*s `{command}{number}{motion}` paradigm never appealed to me because you need to know the number in advance which is rare in my opinion.
It has its advantages though and _Vim_ purists will tell you.
Nevertheless, I soon used [_sneak.vim_](https://github.com/justinmk/vim-sneak) which let me jump to where my eyes rest and entered visual mode for most operations.
The paradigm became "look at start", `{jump}{visual}`, "look at end", `{jump}{command}`.
For text objects it was "look at text object", `{jump}{visual}[ao]{object}{command}`.
It was very ergonomic and I didn't feel overly restricted...

… until I tried to find room for new key mappings.
_Colemak_ necessarily messes up the natural correspondence between key names and commands.
With the default key mappings taking up almost all available keys, it is rather difficult to add key mappings in a consistent manner.
Especially if they are supposed to complement the default commands like simple motions.

Unsatisfied with this situation I started to draft a layout which demands less keys, works well with _Colemak_ and is more expressive.
It took some time for the implementation in Vimscript to take shape and become a real contender.
At some point temptation got me and I switched to it.
After a few months without changes it was time to refactor.
Vimscript and _Vim_ are quirky sometimes and so was the implementation.
I considered reimplementing it in Lua and first attempts were promising.
Eventually, the transition to Lua was completed with almost no quirks left and additional improvements of behavior for many operations.

I use _mynvim_ a lot and don't plan to go back.
