-- bootstrap lazy, see https://github.com/folke/lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)
--


require('lazy').setup({
  {
    'notEvil/mynvim',
    url='https://gitlab.com/notEvil/mynvim',
    dependencies={'easymotion/vim-easymotion'},
    init=function() require('mynvim').initialize() end
  },
})


if false then  -- dein.vim
  -- plugins
  vim.opt.runtimepath:append({'~/.cache/dein/repos/github.com/Shougo/dein.vim'})

  if vim.fn['dein#load_state']('~/.cache/dein') then
    vim.fn['dein#begin']('~/.cache/dein')
    vim.fn['dein#add']('~/.cache/dein/repos/github.com/Shougo/dein.vim')

    vim.fn['dein#add']('https://gitlab.com/notEvil/mynvim')
    vim.fn['dein#add']('easymotion/vim-easymotion')

    vim.fn['dein#end']()
    vim.fn['dein#save_state']()
  end
  --

  require('mynvim').initialize()
end
